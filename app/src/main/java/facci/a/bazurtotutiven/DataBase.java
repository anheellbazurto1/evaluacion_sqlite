package facci.a.bazurtotutiven;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

public class DataBase extends SQLiteOpenHelper {


    private static int version = 1;
    private static String name = "Prueba" ;
    private static SQLiteDatabase.CursorFactory factory = null;

    public DataBase(Context context) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE Universidades(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "Siglas VARCHAR," +
                "NombreUniversidad VARCHAR," +
                "Carreras VARCHAR," +
                "Categoria VARCHAR, " +
                "Ciudad VARCHAR)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Universidades";
        db.execSQL(sql);
        onCreate(db);
    }


    public void Insertar(String Siglas, String NombreU, String Carreras, String Categoria, String Ciudad){

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("Siglas", Siglas);
        contentValues.put("NombreUniversidad", NombreU);
        contentValues.put("Carreras", Carreras);
        contentValues.put("Categoria", Categoria);
        contentValues.put("Ciudad", Ciudad);
        database.insert("Universidades",null,contentValues);
    }



    public void EliminarTodo(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("Universidades", null, null);
    }

    public void Actualizar(String id, String Siglas, String NombreU, String Carreras, String Categoria, String Ciudad){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Siglas", Siglas);
        contentValues.put("NombreUniversidad", NombreU);
        contentValues.put("Carreras", Carreras);
        contentValues.put("Categoria", Categoria);
        contentValues.put("Ciudad", Ciudad);
        database.update("Universidades", contentValues, "id= " + id, null);
    }




    public String LeerTodos() {
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {"id","Siglas", "NombreUniversidad", "Carreras", "Categoria", "Ciudad"};
        String sqlTable = "Universidades";
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                result += "id " + c.getInt(c.getColumnIndex("id")) + "\n" +
                        "Siglas " + c.getString(c.getColumnIndex("Siglas")) + "\n" +
                        "Nombre " + c.getString(c.getColumnIndex("NombreUniversidad")) + "\n" +
                        "Carreras " + c.getString(c.getColumnIndex("Carreras")) + "\n" +
                        "Categoria " + c.getString(c.getColumnIndex("Categoria")) + "\n" +
                        "Ciudad " + c.getString(c.getColumnIndex("Ciudad")) + "\n\n\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }
}
