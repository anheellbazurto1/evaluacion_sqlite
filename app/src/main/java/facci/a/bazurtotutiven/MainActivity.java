package facci.a.bazurtotutiven;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText Siglas, Nombres, Carreras, Categoria, Ciudad, id;
    private Button guardar, leerT, actualizar, eliminarT;
    private TextView Datos;
    private DataBase dataBase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Siglas = (EditText)findViewById(R.id.Siglas);
        Nombres = (EditText)findViewById(R.id.Nombre);
        Carreras = (EditText)findViewById(R.id.Carreras);
        Categoria = (EditText)findViewById(R.id.Categoria);
        Ciudad = (EditText)findViewById(R.id.Ciudad);
        id = (EditText)findViewById(R.id.Id);
        guardar = (Button)findViewById(R.id.Guardar);
        leerT = (Button)findViewById(R.id.LeerT);
        actualizar = (Button)findViewById(R.id.Actualizar);
        eliminarT = (Button)findViewById(R.id.EliminarT);

        guardar.setOnClickListener(this);
        leerT.setOnClickListener(this);
        actualizar.setOnClickListener(this);
        eliminarT.setOnClickListener(this);
        Datos = (TextView)findViewById(R.id.Datos);

        dataBase = new DataBase(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.Guardar:

                if (Siglas.getText().toString().isEmpty()){

                }else if(Siglas.getText().toString().isEmpty()){

                }else if(Nombres.getText().toString().isEmpty()){

                }else if (Carreras.getText().toString().isEmpty()){

                }else if(Categoria.getText().toString().isEmpty()){

                }else if(Ciudad.getText().toString().isEmpty()){

                }else {
                    dataBase.Insertar(Siglas.getText().toString(), Nombres.getText().toString(),
                            Carreras.getText().toString(), Categoria.getText().toString(), Ciudad.getText().toString());
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    Siglas.setText("");
                    Nombres.setText("");
                    Carreras.setText("");
                    Categoria.setText("");
                    Ciudad.setText("");
                    Datos.setText("");
                }

                break;

            case R.id.LeerT:
                Datos.setText(dataBase.LeerTodos());
                id.setText("");
                Siglas.setText("");
                Nombres.setText("");
                Carreras.setText("");
                Categoria.setText("");
                Ciudad.setText("");
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show();
                break;


            case R.id.Actualizar:
                if (Siglas.getText().toString().isEmpty()){

                }else if(Siglas.getText().toString().isEmpty()){

                }else if(Nombres.getText().toString().isEmpty()){

                }else if (Carreras.getText().toString().isEmpty()){

                }else if(Categoria.getText().toString().isEmpty()){

                }else if(Ciudad.getText().toString().isEmpty()){

                }else if(id.getText().toString().isEmpty()){

                }else {
                    dataBase.Actualizar(id.getText().toString(), Siglas.getText().toString(), Nombres.getText().toString(),
                            Carreras.getText().toString(), Categoria.getText().toString(), Ciudad.getText().toString());
                    Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    Siglas.setText("");
                    Nombres.setText("");
                    Carreras.setText("");
                    Categoria.setText("");
                    Ciudad.setText("");
                    Datos.setText("");
                }
                break;

            case R.id.EliminarT:
                dataBase.EliminarTodo();
                Datos.setText("");
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
